from .views_main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from .form_user import *

class MisDatos(Main, View):
	#TODO: add historic
	template = 'profile.html'
	url = "/misDatos/"
	page_title = "My Profile"
	
	def get_data(self, request, kwargs):
		"""
			Gets extra data of the profile
		"""	
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Mis datos", "")
		form_user = formUser(instance = request.user)
		return {"form_user":form_user, "url" : self.url}
	
	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""	
		form_edit = formUser(data = request.POST, instance=request.user)
		if form_edit.is_valid():
			form_edit.save()
			return True
		request.session['last_error'] = form_edit.errors
		return False

class UserView(Main, View):
	#TODO: add historic
	template = 'user.html'
	page_title = "My Profile"
	
	def get_data(self, request, kwargs):
		"""
			Gets extra data of the profile
		"""
		self.url = request.path
		user_id = int(kwargs['pk'])
		user = User.objects.get(pk = user_id)
		
		if not request.user.is_superuser and user != request.user:
			self.can_view_page = False
			return {}
			
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Datos del usuario", "")
		
		self._add_js("usuario.js")
		form_user = formUser(instance = user)
		
		add_producto_form = addProductoForm()
		
		productos = []
		for prod in loginCode.objects.filter(user = user):
			if request.user.is_superuser:
				cod_lec = readCode.objects.get(login_code = prod)
				cod_esc = writeCode.objects.get(login_code = prod)
				productos.append({"pk" : prod.pk, "producto" : prod, "codigo_lectura" : cod_lec, "codigo_escritura" : cod_esc})
			else:
				productos.append({"producto" : prod})
		data_return = {
			"form_user" : form_user,
			"url" : self.url,
			"productos" : productos,
			"selected_user" : user,
			"add_producto_form" : add_producto_form
		}
		return data_return
	
	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""	
		try:
			usuario_id = request.POST.get("usuario", None)
			producto_id = request.POST.get("producto", None)
			usuario = User.objects.get(pk = usuario_id)
			login_code = loginCode.objects.get(Q(pk = producto_id) & Q(user = usuario))
			read_reg = readCode.objects.get(login_code = login_code)
			write_reg = writeCode.objects.get(login_code = login_code)
			
			key = self.get_code(5)
			while loginCode.objects.filter(Q(code = key)):
				key = self.get_code(5)
			login_code.code = key
			login_code.save()
			
			key_read = self.get_code(10)
			while readCode.objects.filter(Q(code = key_read)):
				key_read = self.get_code(10)
			
			read_reg.code = key_read
			read_reg.save()
			
			key_write = self.get_code(15)
			while readCode.objects.filter(Q(code = key_write)):
				key_write = self.get_code(15)
			
			write_reg.code = key_write
			write_reg.save()
			
			return True
		except Exception as e:
			request.session['last_error'] = "Error al editar el producto " + str(e)
		return False
	
	def _removeData(self, request):
		"""
			Edit the object data depending of the form
		"""
		try:
			usuario_id = request.POST.get("usuario", None)
			producto_id = request.POST.get("producto", None)
			usuario = User.objects.get(pk = usuario_id)
			login_code = loginCode.objects.get(Q(pk = producto_id) & Q(user = usuario))
			login_code.delete()
			return True
		except:
			pass
		request.session['last_error'] = "Error al eliminar el producto"
		return False
	
	def get_code(self, nro):
		import string, random
		passkey = ''
		for x in range(nro):
			if random.choice([1,2]) == 1:
				passkey += passkey.join(random.choice(string.ascii_uppercase))
			else:
				passkey += passkey.join(random.choice(string.digits))
		return passkey
	
	def _addData(self, request):
		form_edit = addProductoForm(data = request.POST)
		if form_edit.is_valid():
			producto = form_edit.cleaned_data["producto"]
			usuario_id = request.POST.get("usuario", None)
			usuario = User.objects.get(pk = usuario_id)
			
			if not loginCode.objects.filter(Q(user = usuario) & Q(producto = producto)).exists():
				key = self.get_code(5)
				while loginCode.objects.filter(Q(code = key)):
					key = self.get_code(5)
				
				new_login = loginCode(code = key, user = usuario, producto = producto)
				new_login.save()
				
				key_read = self.get_code(10)
				while readCode.objects.filter(Q(code = key_read)):
					key_read = self.get_code(10)
				
				new_read = readCode(login_code = new_login, code = key_read)
				new_read.save()
				
				key_write = self.get_code(15)
				while readCode.objects.filter(Q(code = key_write)):
					key_write = self.get_code(15)
				
				new_write = writeCode(login_code = new_login, code = key_write)
				new_write.save()
				
				return True
		request.session['last_error'] = form_edit.errors
		return False


class UsuariosView(MainTable, View):
	"""
		view of users
	"""
	page_title = "Usuarios"
	register_name = "usuario"	
	form_action = "/usuarios/"	
	model_object = User
	table_columns =	dict(first_name = "Nombres", last_name = "Apellidos", username = "Username", email = "Correo", fecha_creacion = "Fecha de Ingreso")
	return_edit_columns = ["last_name", "first_name", "email", "celular1", "celular2", "sexo"]
	form_edit = editUser
	form_add = addUser
	delete_register = True
	
	view_aparte = True	
	
	def _preProccess(self, request):
		if request.user.is_superuser:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
			self.can_view = True
		else:
			self.can_view_page = False
	
	def _addData(self, request):
		if self.can_add:
			form_edit = self.form_add(data = request.POST)
			if form_edit.is_valid():
				try:
					username = form_edit.cleaned_data["username"]
					first_name = form_edit.cleaned_data["first_name"]
					last_name = form_edit.cleaned_data["last_name"]
					sexo = form_edit.cleaned_data["sexo"]
					email = form_edit.cleaned_data["email"]
					fecha_nacimiento = form_edit.cleaned_data["fecha_nacimiento"]
					celular1 = form_edit.cleaned_data["celular1"]
					celular2 = form_edit.cleaned_data["celular2"]
					
					new_user = User.objects.create_user(username=username,
				                         email=email)
					new_user.set_password(username)
					new_user.first_name = first_name
					new_user.last_name = last_name
					new_user.sexo = sexo
					new_user.fecha_nacimiento = fecha_nacimiento
					new_user.celular1 = celular1
					new_user.celular2 = celular2
					new_user.save()
					return True
				except Exception as ex:
					print(str(ex))
					request.session['last_error'] = "Ya existe un usuario con estos datos - " +str(ex)
					return False
			request.session['last_error'] = form_edit.errors
		else:
			request.session['last_error'] = "Usted no tiene permisos para añadir este objeto"
		return False
	
	def _get_data_tables(self, request):
		self._add_breadcrumb("Inicio", "/")
		self._add_breadcrumb("Estudiantes", "")
	
	def _getFilerTable(self, value, user):
		fil = Q()
		if value != "":
			fil = fil & (Q(last_name__icontains = value) | Q(first_name__icontains = value) | Q(username__icontains=value) | Q(email__icontains = value) | Q(fecha_creacion__icontains = value))
		return fil
