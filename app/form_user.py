from django import forms
from .models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       
from bootstrap_datepicker_plus import DateTimePickerInput, DatePickerInput
from django_select2.forms import Select2MultipleWidget, HeavySelect2Widget, HeavySelect2MultipleWidget


class formUser(ModelForm):
	
	class Meta:
		model = User
		fields = ["first_name", "last_name", "sexo", "email", "direccion", "celular1", "celular2"]
		widgets = {
			'direccion': Textarea(attrs={'cols': 20, 'rows': 3}),
		}
		

class editAvatar(ModelForm):
	class Meta:
		model = User
		fields = ["avatar"]


class changePass(forms.Form):
	pass1 = forms.CharField(widget=forms.PasswordInput())
	pass2 = forms.CharField(widget=forms.PasswordInput())


class addUser(ModelForm):
	
	class Meta:
		model = User
		fields = ["first_name", "last_name", "username", "sexo", "email", "fecha_nacimiento", "celular1", "celular2"]
		widgets = {
        	'fecha_nacimiento' : DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}
		labels = {
			"email": "E-Mail",
		}
	
class editUser(ModelForm):
	
	class Meta:
		model = User
		fields = ["first_name", "last_name", "sexo", "email", "fecha_nacimiento", "celular1", "celular2"]
		widgets = {
        	'fecha_nacimiento' : DatePickerInput(options={"format":"YYYY-MM-DD"}),
		}
		labels = {
			"email": "E-Mail",
		}

class addProductoForm(ModelForm):
	class Meta:
		model = loginCode
		fields = ["producto"]
		
		
		
		
