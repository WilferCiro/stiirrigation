from app.models import *
from django.http import JsonResponse
from django.views import View
from django.db.models import Q
from django.utils.formats import localize, date_format

def getLastManual(login_code):
	data = riego_fieldManual_last.objects.filter(login_code = login_code).last()
	if data == None:
		data = riego_fieldManual_last(login_code = login_code)
		data.save()
	#fecha_in = str(date_format(data.fecha_ingreso, format='SHORT_DATE_FORMAT', use_l10n=False))
	fecha_in = data.fecha_ingreso
	if fecha_in == None or fecha_in == "" or fecha_in == "null":
		import datetime
		x = datetime.datetime.now()
		fecha_in = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
		data.fecha_ingreso = fecha_in
		data.save()
	in_data = {"concept": "success", "fecha_ingreso" : fecha_in, "field1" : data.campo1}
	return in_data

def getLastAutomatic(login_code):
	data = riego_fieldAutomatico_last.objects.filter(login_code = login_code).last()
	if data == None:
		data = riego_fieldAutomatico_last(login_code = login_code)
		data.save()
	#fecha_in = str(date_format(data.fecha_ingreso, format='SHORT_DATE_FORMAT', use_l10n=False))

	fecha_in = data.fecha_ingreso
	if fecha_in == None or fecha_in == "" or fecha_in == "null":
		import datetime
		x = datetime.datetime.now()
		fecha_in = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
		data.fecha_ingreso = fecha_in
		data.save()

	import datetime
	import pytz
	tz = pytz.timezone('America/Bogota')
	x = datetime.datetime.now(tz = tz)

	field6 = data.campo6

	fecha_server = data.campo9
	if fecha_server != None:
		split_server = fecha_server.split("/")
		fecha_server = datetime.datetime(int(split_server[2]), int(split_server[0]), int(split_server[1]))
		fecha_actual = datetime.datetime(x.year, x.month, x.day)
		dias = fecha_actual - fecha_server
		desface = int(data.days) - int(dias.days)

		if desface <= 0:
			data.campo6 = str(field6[0:2]) + ",0"
		else:
			data.campo6 = str(field6[0:2]) + ",1"

	data.save()
	#fecha_actual = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
	#dias = int(data.days) - (fecha_actual - data.campo9)
	#if field6 == "si,0" or field6 == "no,0":
	#if dias <= 0:
	#	data.campo6 = data.campo6[0:2] + "0"

	field9 = data.campo9
	if data.campo9 == "null" or data.campo9 == None or data.campo9 == "":
		x = datetime.datetime.now()
		field9 = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
		print(x.day)
	in_data = {"concept" : "success", "fecha_ingreso" : fecha_in, "field1" : data.campo1, "field2" : data.campo2, "field3" : data.campo3, "field4" : data.campo4, "field5" : data.campo5, "field6" : data.campo6, "field7" : data.campo7, "field8" : data.campo8, "field9" : field9, "days" : data.days}
	return in_data


def getLastDay(login_code):
	data = riego_fieldAutomatico_last.objects.filter(login_code = login_code).last()
	if data == None:
		data = riego_fieldAutomatico_last(login_code = login_code)
		data.save()
	return data


class ReadView(View):

	def get(self, request, *args, **kargs):
		in_data = {"concept" : "error"}
		only_day = request.GET.get("days_number", None)
		error = False
		try:
			pk = str(kargs['pk'])
			what_data = str(kargs['what_data']).lower()
		except:
			in_data = {"concept" : "error"}
			error = True

		if not error:
			in_data = "error"
			code_valid = None
			try:
				code_valid = readCode.objects.get(code = pk)
			except Exception as ex:
				print(ex)
				in_data = {"concept" : "error"}

			if code_valid != None:
				login_code = code_valid.login_code
				if only_day != None:
					reg = getLastDay(login_code)
					in_data = {"concept" : "success", "dia" : reg.days}
				else:
					if what_data == "manual":
						in_data = getLastManual(login_code)

					elif what_data == "automatico" or what_data == "automático":
						in_data = getLastAutomatic(login_code)

					else:
						in_data = {"concept" : "error"}

		response = JsonResponse(in_data)
		return response



class WriteView(View):

	def get(self, request, *args, **kargs):
		in_data = {"concept" : "error"}
		only_day = request.GET.get("days_number", None)
		error = False
		try:
			pk = str(kargs['pk'])
			what_data = str(kargs['what_data']).lower()
		except:
			in_data = {"concept" : "error"}
			error = True

		if not error:
			in_data = "error"
			code_valid = None
			try:
				code_valid = writeCode.objects.get(code = pk)
			except:
				in_data = {"concept" : "error"}

			if code_valid != None:
				login_code = code_valid.login_code
				if only_day != None:
					reg = getLastDay(login_code)
					dias = request.GET.get("dias", reg.days)
					reg.days = dias
					reg.save()
					if riego_fieldAutomatico_last.objects.filter(login_code = login_code).count() > 10:
						first = riego_fieldAutomatico_last.objects.filter(login_code = login_code).first()
						first.delete()
					in_data = {"concept" : "success"}
				else:
					if what_data == "manual":
						in_data = getLastManual(login_code)
						field1 = request.GET.get("field1", in_data["field1"])
						reg = riego_fieldManual_last(campo1 = field1, login_code = login_code, nombre = "Registro")
						reg.save()
						in_data = {"concept" : "success"}

						if riego_fieldManual_last.objects.filter(login_code = login_code).count() > 10:
							first = riego_fieldManual_last.objects.filter(login_code = login_code).first()
							first.delete()


					elif what_data == "automatico":
						in_data = getLastAutomatic(login_code)
						
						field6_data = ""
						if in_data["field6"] != None:
							field6_data = in_data["field6"][-1]
							

						field1 = request.GET.get("field1", in_data["field1"])
						field2 = request.GET.get("field2", in_data["field2"])
						field3 = request.GET.get("field3", in_data["field3"])
						field4 = request.GET.get("field4", in_data["field4"])
						field5 = request.GET.get("field5", in_data["field5"])

						field6 = request.GET.get("field6", in_data["field6"])
						field6 = field6[0:3] + field6_data
						field7 = request.GET.get("field7", in_data["field7"])
						field8 = request.GET.get("field8", in_data["field8"])
						field9 = request.GET.get("field9", in_data["field9"])
						days = request.GET.get("days", in_data["days"])
						if field9 == "null" or field9 == None or field9 == "":

							import datetime
							import pytz
							tz = pytz.timezone('America/Bogota')
							x = datetime.datetime.now(tz = tz)
							field9 = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
						reg = riego_fieldAutomatico_last(login_code = login_code, nombre = "Registro")
						reg.campo1 = field1
						reg.campo2 = field2
						reg.campo3 = field3
						reg.campo4 = field4
						reg.campo5 = field5
						reg.campo6 = field6
						reg.campo7 = field7
						reg.campo8 = field8
						reg.campo9 = field9
						reg.days = days
						reg.save()

						if riego_fieldAutomatico_last.objects.filter(login_code = login_code).count() > 10:
							first = riego_fieldAutomatico_last.objects.filter(login_code = login_code).first()
							first.delete()
						in_data = {"concept" : "success"}

		response = JsonResponse(in_data)
		response["Access-Control-Allow-Origin"] = "*"
		response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
		response["Access-Control-Max-Age"] = "1000"
		response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
		return response

class LoginRequest(View):

	def encripta(self, data):
		new_string = ""
		for s in data:
			new_string += chr(ord(s)-3)
		return new_string

	def desencripta(self, data):
		new_string = ""
		for s in data:
			new_string += chr(ord(s)+3)
		return new_string

	def get(self, request, *args, **kargs):
		in_data = {"concept" : "error"}
		error = False
		ya = request.GET.get("ya", None)
		try:
			username = self.desencripta(str(kargs['username']).replace("_-_-", "/").replace("____", "?"))
			password = self.desencripta(str(kargs['pass']).replace("_-_-", "/").replace("____", "?"))
		except:
			in_data = {"concept" : "error"}
			error = True
		if not error:
			in_data = {"concept" : "error"}
			error = False
			try:
				l_code = loginCode.objects.get(code = password)
				if l_code.user.username == username:
					try:
						write = writeCode.objects.get(login_code = l_code)
					except writeCode.DoesNotExist:
						write = writeCode(login_code = l_code, code = "one")
						error = True
					try:
						read = readCode.objects.get(login_code = l_code)
					except readCode.DoesNotExist:
						read = readCode(login_code = l_code, code = "one")
						error = True
					if not error:
						if l_code.nro_inicio < 5:
							if ya == None:
								l_code.nro_inicio = l_code.nro_inicio + 1
								l_code.save()
							in_data = {"concept" : "success", "write_code" : self.encripta(write.code), "read_code" : self.encripta(read.code), "product" : l_code.producto.app_code, "nro" : l_code.nro_inicio}
						else:
							in_data = {"concept" : "error_full"}
					else:
						in_data = {"concept" : "error"}
				else:
					in_data = {"concept" : "error"}
			except Exception as e:
				print(e)
				in_data = {"concept" : "error"}

		return JsonResponse(in_data)

class LogoutView(View):

	def get(self, request, *args, **kargs):
		error = False
		try:
			password = str(kargs['pass']).replace("_-_-", "/").replace("____", "?")
			usuario = str(kargs['usuario']).replace("_-_-", "/").replace("____", "?")
		except:
			in_data = {"concept" : "error_get_data"}
			error = True

		if not error:
			l_code = loginCode.objects.get(code = password)
			if l_code.user.username == usuario:
				if l_code.nro_inicio - 1 >= 0:
					l_code.nro_inicio = l_code.nro_inicio - 1
					l_code.save()
				in_data = {"concept" : "success"}
			#in_data = {"concept" : "error_in " + password + " - " + usuario + " - " + l_code.user.username}

		return JsonResponse(in_data)

		
