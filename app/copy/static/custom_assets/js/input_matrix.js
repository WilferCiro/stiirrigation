$(document).ready(function(){
	const im = new input_m();
});


class input_m extends PageHandler {
	constructor() {
		super();
		$("#viewControlsCheck").change({self : this}, this.toggleControls);
		$(".controlTextarea").keyup({self : this}, this.saveValue);
		
		$(".deleteValue").click(this.deleteValue);
		$(".deleteParameter").click(this.deleteParameter);
		
		$(".formSendAjax").submit({self : this}, this.submitFormAjax);
		
		$("button, div.value").tooltip();
		$("#showHelp").click(function(){
			$("div.value").tooltip("toggle");
		});
		
		$(".addValueButton").click(self.addValueModal);
		$(".editObservationsParameter").click(self.editObservationsParameter);
		$(".editObservationsValue").click(self.editObservationsValue);
		
		$(".edit_permission").click(self.edit_permission);
	}
	
	addValueModal(event) {
		var parameter_id = $(this).attr("parameter_id");
		$("#add-value").modal("show");
		$("#parameter_id_add_value").attr("value", parameter_id);
	}
	
	editObservationsParameter(event) {
		var parameter_id = $(this).attr("parameter_id");
		var text = $(this).attr("observations");
		$("#edit-observations").modal("show");
		$("#observations_field").text(text);
		$("#parameter_observations_id").attr("value", parameter_id);
		$("#tipoEditObservations").attr("value", "parameter_observations");
	}
	
	editObservationsValue(event) {
		var parameter_id = $(this).attr("parameter_id");
		var value_id = $(this).attr("value_id");
		var text = $(this).attr("observations");
		$("#edit-observations").modal("show");
		$("#observations_field").text(text);
		$("#parameter_observations_id").attr("value", parameter_id);
		$("#value_observations_id").attr("value", value_id);
		$("#tipoEditObservations").attr("value", "value_observations");
	}
	
	
	deleteParameter (event) {
		var parameter_id = $(this).attr("parameter_id");
		$("#parameter_id_delete").attr("value", parameter_id);
		$("#tipoDelete").attr("value", "parameter");
		$('#delete-model-modal').modal('show');
	}
	
	deleteValue (event) {
		var parameter_id = $(this).attr("parameter_id");
		var value_id = $(this).attr("value_id");
		$("#parameter_id_delete").attr("value", parameter_id);
		$("#value_id_delete").attr("value", value_id);
		$("#tipoDelete").attr("value", "value");
		$('#delete-model-modal').modal('show');
	}
	
	
	toggleControls (event){
		var checked = $(this).prop('checked');
		if (checked == true){
			$(".valuesOptions").show(200);
			$("#viewControlsCheckLabel").text("Hide value controls");
		}
		else{
			$(".valuesOptions").hide(200);
			$("#viewControlsCheckLabel").text("Show value controls");
		}
	}
	
	edit_permission (event){
		event.preventDefault();
		var pk = $(this).attr("pk");
		var can_view = $("#can_view"+pk).prop('checked');
		var can_edit = $("#can_edit"+pk).prop('checked');
		var project_id = $("#project_id").val();
		var data = {ajaxRequest : "Yes", ajax_action : "editData", tipoEdit: "permission", can_view : can_view, can_edit : can_edit, pk : pk, project_id : project_id}
		var url = $("#form_for_action").attr("action");
		self.sendData(self, data, [], url);
	}
	
	saveValue (event){
		event.preventDefault();
		var self = event.data.self;
		if (event.keyCode == 13){
			var value = $(this).val().replace("\n", "");
			$(this).val(value);
			if (value == ""){
				alert("Por favor llene el campo");
				return;
			}
			var url = $("#form_for_action").attr("action");
			var action = $(this).attr("action");
			var data;
			var project_id = $("#project_id").val();		
			
			if(action == "Value"){
				var value_id = $(this).attr("value_id");
				var parameter_id = $(this).attr("parameter_id");
				data = {ajaxRequest : "Yes", ajax_action : "editData", tipoEdit: "value_title", project_id : project_id, value_id : value_id, parameter_id : parameter_id, title : value};
			}
			else if(action == "Parameter"){
				var parameter_id = $(this).attr("parameter_id");		
				data = {ajaxRequest : "Yes", ajax_action : "editData", tipoEdit: "parameter_title", project_id : project_id, parameter_id : parameter_id, title : value};
			}
			self.sendData(self, data, [], url);
		}
	}
	
	
}


