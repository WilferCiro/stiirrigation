from app.models import *
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect
from app.forms_login import *
from app.mails import send_an_email

class SingUp(View):
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		form = SingUpForm()

		return render(request, "singup.html", {"form" : form})

	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")

		form = SingUpForm(data = request.POST)
		error = ""
		if form.is_valid():
			apellido = form.cleaned_data["apellido"]
			nombre = form.cleaned_data["nombre"]
			documento = form.cleaned_data["documento"]
			email = form.cleaned_data["email"]
			pass1 = form.cleaned_data["pass1"]
			pass2 = form.cleaned_data["pass2"]
			sexo = form.cleaned_data["sexo"]
			phone1 = form.cleaned_data["phone1"]
			producto = None
			producto = form.cleaned_data["producto"]
			if pass1 != pass2:
				error = "Las contraseñas no coinciden"
			else:
				try:
					exists = User.objects.get(username = documento)
					error = "No se puede registrar este documento, revise si ya está registrado"
				except:
					try:
						new_user = User.objects.create_user(username=documento,
							                 email=email)
						new_user.set_password(pass1)
						new_user.es_estudiante = True
						new_user.es_administrativo = False
						new_user.last_name = apellido
						new_user.first_name = nombre
						new_user.documento = documento
						new_user.sexo = sexo
						new_user.celular1 = phone1
						new_user.save()

						asunto = "Registro de nuevo usuario"
						cuerpo = "Se ha registrado un nuevo usuario, " + str(nombre) + " " + str(apellido) + ", solicitando el producto: " + str(producto.titulo) + ", para ver su perfil visita el siguiente link <a href='https://stiirrigation.com/usuarios/view/" + str(new_user.pk) + "'>https://stiirrigation.com/usuarios/view/" + str(new_user.pk) + "</a>"

						mails = []
						for use in User.objects.filter(Q(is_superuser = True)):
							mails.append(use.email)

						send_an_email(asunto, cuerpo, mails)

						return HttpResponseRedirect("/index?logged")
					except Exception as ex:
						print(ex)
						error = "error en el registro"
						pass
		print(form.errors)
		return render(request, "singup.html", {"form" : form, "error" : error})


class ServicioCliente(View):
	def get(self, request, *args, **kvargs):
		success = request.GET.get("success", None)

		form = ServicioClienteForm()

		return render(request, "servicio_cliente.html", {"form" : form, "success" : success})

	def post(self, request, *args, **kvargs):
		form = ServicioClienteForm(request.POST, request.FILES)
		error = ""
		if form.is_valid():
			nombre = form.cleaned_data["nombre"]
			apellido = form.cleaned_data["apellido"]
			email = form.cleaned_data["email"]
			telefono = form.cleaned_data["telefono"]
			asunto = form.cleaned_data["asunto"]
			pregunta = form.cleaned_data["pregunta"]
			tipo = form.cleaned_data["tipo"]
			try:
				imagen = request.FILES["imagen"]
			except:
				imagen = None
			direccion = form.cleaned_data["direccion"]

			form.save()

			ser = servicio_cliente.objects.last()

			asunto = "[Servicio al cliente] " + "[" + str(tipo) + "] " + str(asunto)
			cuerpo = "Mensaje de: " + str(nombre) + " " + str(apellido) + " (Telefono: " + str(telefono) + ", Email: " + str(email) + ", Dirección: " + str(direccion) + ")<br /><br/>" + str(pregunta)
			if ser.imagen != None and ser.imagen != "":
				cuerpo += "<img src='https://www.stiirrigation.com" + str(ser.imagen.url) + "'>"
			mails = []
			for use in User.objects.filter(Q(is_superuser = True)):
				mails.append(use.email)

			send_an_email(asunto, cuerpo, mails)

			return HttpResponseRedirect("/servicio_cliente?success")

		return render(request, "servicio_cliente.html", {"form" : form, "error" : error})
	
