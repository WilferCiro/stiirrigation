from .views_main import Main, MainTable
from django.shortcuts import render
from django.views import View
from .models import *
from django.conf import settings
# Create your views here.

import json

from django.contrib import messages
from django.http import HttpResponse, HttpResponseRedirect
from django.shortcuts import redirect, render

from django.utils.translation import ugettext_lazy as _

from django.utils import translation

class IndexPage(Main, View):
	template = "index.html"
	page_title = _("Inicio")
	def get_data(self, request, args):
		self._add_breadcrumb(_("Inicio"), "")

		users_number = User.objects.filter(Q(is_superuser = False)).count()
		productos_number = Producto.objects.all().count()

		list_products = [] #Producto.objects.all()
		for prod in Producto.objects.all():
			name = prod.titulo
			try:
				lang = request.session[translation.LANGUAGE_SESSION_KEY]
			except:
				lang = "es"
			if lang == "en":
				name = name.replace("próximamente", "next")
			list_products.append({
				"titulo" : name,
				"descripcion" : prod.descripcion,
				"url" : prod.url_app
			})
		data_return = {
			"users_number" : users_number,
			"productos_number" : productos_number,
			"list_products" : list_products,
		}

		return data_return

def changeLanguage(request):
	language = request.POST.get("lang", "es")
	translation.activate(language)
	request.session[translation.LANGUAGE_SESSION_KEY] = language
	return HttpResponseRedirect(request.META.get('HTTP_REFERER'))

def logoutView (request):
	"""
		Logout from application, and returns to blog
	"""
	from django.contrib.auth import logout
	logout(request)
	return HttpResponseRedirect('/index/?success')

def loginView (request):
	"""
		Log in the page
	"""
	from django.contrib.auth import authenticate, login
	username = request.POST.get("usuario", None)
	password = request.POST.get("contrasena", None)
	user = authenticate(username=username, password=password)
	try:
		login(request, user)
		return HttpResponseRedirect(request.META.get('HTTP_REFERER'))
	except :
		return HttpResponseRedirect('/index/?error')

		
