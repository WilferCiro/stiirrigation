from django.contrib import admin
from app.models import *
from django.contrib.auth.admin import UserAdmin

# Register your models here.
admin.site.register(Producto)
admin.site.register(ProductoFoto)
admin.site.register(User, UserAdmin)

admin.site.register(Pais)
admin.site.register(riego_fieldAutomatico_last)
#admin.site.register(riego_fieldAutomatico_old)
admin.site.register(riego_fieldManual_last)
#admin.site.register(riego_fieldManual_old)
admin.site.register(readCode)
admin.site.register(writeCode)

admin.site.register(loginCode)

admin.site.register(servicio_cliente)
