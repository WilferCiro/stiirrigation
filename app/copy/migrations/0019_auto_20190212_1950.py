# Generated by Django 2.1.5 on 2019-02-13 00:50

from django.db import migrations, models


class Migration(migrations.Migration):

    dependencies = [
        ('app', '0018_riego_fieldautomatico_last_days'),
    ]

    operations = [
        migrations.AlterField(
            model_name='riego_fieldautomatico_last',
            name='fecha_ingreso',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
        migrations.AlterField(
            model_name='riego_fieldmanual_last',
            name='fecha_ingreso',
            field=models.CharField(blank=True, max_length=100, null=True),
        ),
    ]
