from django.urls import path
from app import views
from app import views_request_json, views_user, views_producto, views_login


urlpatterns = [
    path('logout/', views.logoutView, name='Logout'),
    path('login/', views.loginView, name='Login'),
    path('index/', views.IndexPage.as_view(), name='IndexPage'),
    path('', views.IndexPage.as_view(), name='IndexPage'),
    path('inicio/', views.IndexPage.as_view(), name='IndexPage'),

    path('usuarios/', views_user.UsuariosView.as_view(), name='UsuariosView'),
    path('usuarios/view/<pk>/', views_user.UserView.as_view(), name='UserView'),

    path('read/<pk>/<what_data>/', views_request_json.ReadView.as_view(), name='ReadView'),
    path('write/<pk>/<what_data>/', views_request_json.WriteView.as_view(), name='WriteView'),
    path('logout/app/<pass>/<usuario>/', views_request_json.LogoutView.as_view(), name='LogoutView'),
    
    path('foto/<username>/<pass>/<zona>', views_request_json.getFoto.as_view(), name='getFoto'),

    path('login/<username>/<pass>/', views_request_json.LoginRequest.as_view(), name='loginView'),

    path("especificaciones/", views_producto.EspecificacionesView.as_view(), name="EspecificacionesView"),

    path("singup/", views_login.SingUp.as_view(), name="SingUp"),
	path("recover/pass", views_login.RecoverPass.as_view(), name="RecoverPass"),
	path("recover/pass/code", views_login.RecoverPassCode.as_view(), name="RecoverPassCode"),
	
    path("servicio_cliente/", views_login.ServicioCliente.as_view(), name="ServicioCliente"),

    path("change/lang/", views.changeLanguage, name="changeLanguage"),
]

