from .views_main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from .form_productos import *
from django.utils.translation import ugettext_lazy as _

class EspecificacionesView(MainTable, View):
	"""
		Vista de los productos disponibles
	"""
	page_title = _("Productos")
	register_name = _("producto")
	form_action = "/especificaciones/"
	model_object = Producto
	
	table_columns = dict(titulo = _("Título"), tags = _("Etiquetas"), fecha_creacion = _("Fecha creación"), app_code = _("Código App"), url_app = _("URL play store"))
	return_edit_columns = ["titulo", "tags", "descripcion", "app_code", "url_app"]
	form_edit = editProducto
	form_add = editProducto
	edit_aparte = True
	
	def _get_data_tables(self, request):
		# Breadcrumb organization
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Productos"), "")
	
	def _getFilerTable(self, value, user):
		if value != "":
			fil = (Q(titulo__icontains = value) | Q(tags__icontains = value) | Q(fecha_creacion__icontains = value) | Q(app_code__icontains = value) | Q(url_app__icontains = value))
			return fil
		return Q()
		
	def _preProccess(self, request):
		if request.user.is_superuser:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
		else:
			self.can_view_page = False
			
			
