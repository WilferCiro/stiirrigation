from django.core.mail import send_mail

def send_an_email(asunto = "asunto", mensaje = "mensaje", correos = []):
	
	send_mail(
		asunto,
		'',
		'riego@stiirrigation.com',
		correos,
		fail_silently=False,
		html_message = mensaje
	)
	return True
