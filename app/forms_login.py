from django import forms
from app.models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       

class SingUpForm(forms.Form):
	nombre = forms.CharField(label="Nombre * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	apellido = forms.CharField(label="Apellido * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	documento = forms.CharField(label="Nombre de usuario * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	email = forms.EmailField(label="E-mail * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	phone1 = forms.CharField(label="Teléfono * ", required=True, widget=forms.TextInput(attrs={'placeholder': ''}))
	pass1 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': ''}), label="Contraseña * ", required=True)
	pass2 = forms.CharField(widget=forms.PasswordInput(attrs={'placeholder': ''}), label="Repita contraseña * ", required=True)
	sexo = forms.ChoiceField(label="", required=True, choices = User.SEX_CHOICES)
	producto = forms.ModelChoiceField(label="Producto deseado *", queryset = Producto.objects.all())


class ServicioClienteForm(ModelForm):
	class Meta:
		model = servicio_cliente
		fields = "__all__"
		widgets = {
			'mensaje': Textarea(attrs={'cols': 20, 'rows': 3})
		}
		
