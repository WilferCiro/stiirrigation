
from django import forms
from .models import *
from django.forms import ModelForm, Textarea
from django.contrib.admin import widgets                                       
from bootstrap_datepicker_plus import DateTimePickerInput, DatePickerInput
from django_select2.forms import Select2MultipleWidget, HeavySelect2Widget, HeavySelect2MultipleWidget


class editProducto(ModelForm):
	
	class Meta:
		model = Producto
		fields = ["titulo", "app_code", "url_app", "tags", "descripcion"]
		widgets = {
			'descripcion': Textarea(attrs={'cols': 20, 'rows': 3}),
			'tags': Textarea(attrs={'cols': 20, 'rows': 3}),
		}
		
