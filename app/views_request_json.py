from app.models import *
from django.http import JsonResponse
from django.views import View
from django.db.models import Q
from django.utils.formats import localize, date_format
from django.shortcuts import HttpResponse
import os

def getLastManual(login_code, fromCel = False):
	data = riego_fieldManual_last.objects.filter(login_code = login_code).last()
	if data == None:
		data = riego_fieldManual_last(login_code = login_code)
		data.save()
	#fecha_in = str(date_format(data.fecha_ingreso, format='SHORT_DATE_FORMAT', use_l10n=False))
	fecha_in = data.fecha_ingreso
	import datetime
	x = datetime.datetime.now()
	date_read = data.descripcion
	if not fromCel:
		date_read = str(x.month).zfill(2) + "/" + str(x.day).zfill(2) + "/" + str(x.year) + "-" + str(x.hour).zfill(2) + ":" + str(x.minute).zfill(2) + ":" + str(x.second).zfill(2)
		data.descripcion = date_read
	if fecha_in == None or fecha_in == "" or fecha_in == "null":
		import datetime
		x = datetime.datetime.now()
		fecha_in = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
		data.fecha_ingreso = fecha_in
		data.save()
	in_data = {"concept": "success", "fecha_ingreso" : fecha_in, "field1" : data.campo1, "date_read" : date_read}
	return in_data

def getLastAutomatic(login_code, fromCel = False):
	data = riego_fieldAutomatico_last.objects.filter(login_code = login_code).last()
	if data == None:
		data = riego_fieldAutomatico_last(login_code = login_code)
		data.save()
	#fecha_in = str(date_format(data.fecha_ingreso, format='SHORT_DATE_FORMAT', use_l10n=False))

	fecha_in = data.fecha_ingreso
	if fecha_in == None or fecha_in == "" or fecha_in == "null":
		import datetime
		x = datetime.datetime.now()
		fecha_in = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
		data.fecha_ingreso = fecha_in
		data.save()

	import datetime
	import pytz
	tz = pytz.timezone('America/Bogota')
	x = datetime.datetime.now(tz = tz)

	field6 = data.campo6

	fecha_server = data.campo9
	if fecha_server != None:
		split_server = fecha_server.split("/")
		fecha_server = datetime.datetime(int(split_server[2]), int(split_server[0]), int(split_server[1]))
		fecha_actual = datetime.datetime(x.year, x.month, x.day)
		dias = fecha_actual - fecha_server
		desface = int(data.days) - int(dias.days)
		if field6 != None:
			if desface <= 0:
				data.campo6 = str(field6[0:2]) + ",0"
			else:
				data.campo6 = str(field6)
		else:
			data.campo6 = ""
			
	x = datetime.datetime.now()
	date_read = data.descripcion
	if not fromCel:
		date_read = str(x.month).zfill(2) + "/" + str(x.day).zfill(2) + "/" + str(x.year) + "-" + str(x.hour).zfill(2) + ":" + str(x.minute).zfill(2) + ":" + str(x.second).zfill(2)
		data.descripcion = date_read

	data.save()
	#fecha_actual = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
	#dias = int(data.days) - (fecha_actual - data.campo9)
	#if field6 == "si,0" or field6 == "no,0":
	#if dias <= 0:
	#	data.campo6 = data.campo6[0:2] + "0"

	field9 = data.campo9
	if data.campo9 == "null" or data.campo9 == None or data.campo9 == "":
		field9 = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
	in_data = {"concept" : "success", "fecha_ingreso" : fecha_in, "field1" : data.campo1, "field2" : data.campo2, "field3" : data.campo3, "field4" : data.campo4, "field5" : data.campo5, "field6" : data.campo6, "field7" : data.campo7, "field8" : data.campo8, "field9" : field9, "field10" : data.campo10, "field11" : data.campo11, "days" : data.days, "hora" : str(x.weekday()) + "/" + str(x.hour) + "/" + str(x.minute) + "/" + str(x.second), "date_read" : date_read}
	
	if (data.login_code.producto.app_code == "1z"):
		in_data["bat1"] = data.bat1
		in_data["bat2"] = data.bat2
		in_data["bat3"] = data.bat3
		in_data["bat4"] = data.bat4
		in_data["bat5"] = data.bat5
		in_data["bat6"] = data.bat6
		in_data["bat7"] = data.bat7
		in_data["bat8"] = data.bat8
		in_data["bat9"] = data.bat9
		in_data["bat10"] = data.bat10
		in_data["bat11"] = data.bat11
		in_data["bat12"] = data.bat12
		in_data["bat13"] = data.bat13
		in_data["bat14"] = data.bat14
		in_data["bat15"] = data.bat15
		in_data["bat16"] = data.bat16
		in_data["bat17"] = data.bat17
		in_data["bat18"] = data.bat18
		in_data["bat19"] = data.bat19
		in_data["bat20"] = data.bat20
		in_data["bat21"] = data.bat21
	
	return in_data


def getLastDay(login_code):
	data = riego_fieldAutomatico_last.objects.filter(login_code = login_code).last()
	if data == None:
		data = riego_fieldAutomatico_last(login_code = login_code)
		data.save()
	return data


class ReadView(View):

	def get(self, request, *args, **kargs):
		in_data = {"concept" : "error"}
		only_day = request.GET.get("days_number", None)
		only_now = request.GET.get("now", None)
		get_active_valvule = request.GET.get("get_active_valvule", None)
		from_cel = request.GET.get("from_cel", None)
		if from_cel == None:
			from_cel = False
		else:
			from_cel = True
		error = False
		try:
			pk = str(kargs['pk'])
			what_data = str(kargs['what_data']).lower()
		except:
			in_data = {"concept" : "error"}
			error = True

		if not error:
			in_data = "error"
			code_valid = None
			try:
				code_valid = readCode.objects.get(code = pk)
			except Exception as ex:
				print(ex)
				in_data = {"concept" : "error"}

			if code_valid != None:
				login_code = code_valid.login_code
				if only_day != None:
					reg = getLastDay(login_code)
					in_data = {"concept" : "success", "dia" : reg.days}
				elif only_now != None:
					import datetime
					x = datetime.datetime.now()
					hora = x.hour * 100 + x.minute
					in_data = {"concept" : "success", "dia" : hora}
				elif get_active_valvule != None:
					# TODO: IMPLEMENTAR CODIGO COMPLETO
					in_data = {"concept" : "success", "valvule": "6"}
				else:
					if what_data == "manual":
						in_data = getLastManual(login_code, fromCel = from_cel)

					elif what_data == "automatico" or what_data == "automático":
						in_data = getLastAutomatic(login_code, fromCel = from_cel)

					else:
						in_data = {"concept" : "error"}

		response = JsonResponse(in_data)
		return response



class WriteView(View):

	def get(self, request, *args, **kargs):
		in_data = {"concept" : "error"}
		only_day = request.GET.get("days_number", None)
		error = False
		try:
			pk = str(kargs['pk'])
			what_data = str(kargs['what_data']).lower()
		except:
			in_data = {"concept" : "error"}
			error = True

		if not error:
			in_data = "error"
			code_valid = None
			try:
				code_valid = writeCode.objects.get(code = pk)
			except:
				in_data = {"concept" : "error"}

			if code_valid != None:
				login_code = code_valid.login_code
				if only_day != None:
					reg = getLastDay(login_code)
					dias = request.GET.get("dias", reg.days)
					reg.days = dias
					reg.save()
					if riego_fieldAutomatico_last.objects.filter(login_code = login_code).count() > 10:
						first = riego_fieldAutomatico_last.objects.filter(login_code = login_code).first()
						first.delete()
					in_data = {"concept" : "success"}
				else:
					if what_data == "manual":
						in_data = getLastManual(login_code, fromCel=True)
						field1 = request.GET.get("field1", in_data["field1"])
						reg = riego_fieldManual_last(campo1 = field1, login_code = login_code, nombre = "Registro")
						reg.save()
						in_data = {"concept" : "success"}

						if riego_fieldManual_last.objects.filter(login_code = login_code).count() > 10:
							first = riego_fieldManual_last.objects.filter(login_code = login_code).first()
							first.delete()


					elif what_data == "automatico":
						in_data = getLastAutomatic(login_code, fromCel=True)
						
						field6_data = ""
						if in_data["field6"] != None:
							field6_data = in_data["field6"]
						if field6_data == None:
							field6_data = ""
						else:
							if len(field6_data) > 0:
								field6_data = field6_data[-1]								
						
						if (login_code.producto.app_code == "1z"):
							bat1 = request.GET.get("bat1", in_data["bat1"])
							bat2 = request.GET.get("bat2", in_data["bat2"])
							bat3 = request.GET.get("bat3", in_data["bat3"])
							bat4 = request.GET.get("bat4", in_data["bat4"])
							bat5 = request.GET.get("bat5", in_data["bat5"])
							bat6 = request.GET.get("bat6", in_data["bat6"])
							bat7 = request.GET.get("bat7", in_data["bat7"])
							bat8 = request.GET.get("bat8", in_data["bat8"])
							bat9 = request.GET.get("bat9", in_data["bat9"])
							bat10 = request.GET.get("bat10", in_data["bat10"])
							bat11 = request.GET.get("bat11", in_data["bat11"])
							bat12 = request.GET.get("bat12", in_data["bat12"])
							bat13 = request.GET.get("bat13", in_data["bat13"])
							bat14 = request.GET.get("bat14", in_data["bat14"])
							bat15 = request.GET.get("bat15", in_data["bat15"])
							bat16 = request.GET.get("bat16", in_data["bat16"])
							bat17 = request.GET.get("bat17", in_data["bat17"])
							bat18 = request.GET.get("bat18", in_data["bat18"])
							bat19 = request.GET.get("bat19", in_data["bat19"])
							bat20 = request.GET.get("bat20", in_data["bat20"])
							bat21 = request.GET.get("bat21", in_data["bat21"])							

						field1 = request.GET.get("field1", in_data["field1"])
						field2 = request.GET.get("field2", in_data["field2"])
						field3 = request.GET.get("field3", in_data["field3"])
						field4 = request.GET.get("field4", in_data["field4"])
						field5 = request.GET.get("field5", in_data["field5"])

						field6 = request.GET.get("field6", in_data["field6"])
						if field6 != None:
						    field6 = field6[0:3] + field6_data
						else:
						    field6 = "no,0"
						field7 = request.GET.get("field7", in_data["field7"])
						field8 = request.GET.get("field8", in_data["field8"])
						field9 = request.GET.get("field9", in_data["field9"])
						days = request.GET.get("days", in_data["days"])
						if field9 == "null" or field9 == None or field9 == "":

							import datetime
							import pytz
							tz = pytz.timezone('America/Bogota')
							x = datetime.datetime.now(tz = tz)
							field9 = str(x.month) + "/" + str(x.day) + "/" + str(x.year)
						reg = riego_fieldAutomatico_last(login_code = login_code, nombre = "Registro")
						reg.campo1 = field1
						reg.campo2 = field2
						reg.campo3 = field3
						reg.campo4 = field4
						reg.campo5 = field5
						reg.campo6 = field6
						reg.campo7 = field7
						reg.campo8 = field8
						reg.campo9 = field9
						reg.descripcion = in_data["date_read"]
						reg.days = days
						
						if (login_code.producto.app_code == "1z"):
							reg.bat1 = bat1
							reg.bat2 = bat2
							reg.bat3 = bat3
							reg.bat4 = bat4
							reg.bat5 = bat5
							reg.bat6 = bat6
							reg.bat7 = bat7
							reg.bat8 = bat8
							reg.bat9 = bat9
							reg.bat10 = bat10
							reg.bat11 = bat11
							reg.bat12 = bat12
							reg.bat13 = bat13
							reg.bat14 = bat14
							reg.bat15 = bat15
							reg.bat16 = bat16
							reg.bat17 = bat17
							reg.bat18 = bat18
							reg.bat19 = bat19
							reg.bat20 = bat20
							reg.bat21 = bat21						
						
						reg.save()

						if riego_fieldAutomatico_last.objects.filter(login_code = login_code).count() > 10:
							first = riego_fieldAutomatico_last.objects.filter(login_code = login_code).first()
							first.delete()
						in_data = {"concept" : "success"}

		response = JsonResponse(in_data)
		response["Access-Control-Allow-Origin"] = "*"
		response["Access-Control-Allow-Methods"] = "GET, OPTIONS"
		response["Access-Control-Max-Age"] = "1000"
		response["Access-Control-Allow-Headers"] = "X-Requested-With, Content-Type"
		return response


class LoginRequest(View):

	def encripta(self, data):
		new_string = ""
		for s in data:
			new_string += chr(ord(s)-3)
		return new_string

	def desencripta(self, data):
		new_string = ""
		for s in data:
			new_string += chr(ord(s)+3)
		return new_string

	def get(self, request, *args, **kargs):
		in_data = {"concept" : "error"}
		error = False
		ya = request.GET.get("ya", None)
		try:
			username = self.desencripta(str(kargs['username']).replace("_-_-", "/").replace("____", "?"))
			password = self.desencripta(str(kargs['pass']).replace("_-_-", "/").replace("____", "?"))
		except:
			in_data = {"concept" : "error"}
			error = True
		if not error:
			in_data = {"concept" : "error"}
			error = False
			try:
				
				l_code = loginCode.objects.get(code = password)
				if l_code.user.username == username:
					try:
						write = writeCode.objects.get(login_code = l_code)
					except writeCode.DoesNotExist:
						write = writeCode(login_code = l_code, code = "one")
						error = True
					try:
						read = readCode.objects.get(login_code = l_code)
					except readCode.DoesNotExist:
						read = readCode(login_code = l_code, code = "one")
						error = True
					if not error:
						if l_code.nro_inicio < 5 or ya != None:
							if ya == None:
								l_code.nro_inicio = l_code.nro_inicio + 1
								l_code.save()

							in_data = {"concept" : "success", "write_code" : self.encripta(write.code), "read_code" : self.encripta(read.code), "product" : l_code.producto.app_code, "nro" : l_code.nro_inicio}
							
							all_codes = loginCode.objects.filter(user = l_code.user)
							
							for cod in all_codes:
								try:
									write_new = writeCode.objects.get(login_code = cod)
								except writeCode.DoesNotExist:
									write_new = writeCode(login_code = cod, code = "one")
									error = True
								in_data["write_" + cod.producto.app_code] = write_new.code
								try:
									read_new = readCode.objects.get(login_code = cod)
								except readCode.DoesNotExist:
									read_new = readCode(login_code = cod, code = "one")
									error = True
								in_data["read_" + cod.producto.app_code] = read_new.code
						else:
							in_data = {"concept" : "error_full"}
					else:
						in_data = {"concept" : "error"}
				else:
					in_data = {"concept" : "error"}
			except Exception as e:
				print(e)
				in_data = {"concept" : "error"}

		return JsonResponse(in_data)

class LogoutView(View):

	def get(self, request, *args, **kargs):
		error = False
		try:
			password = str(kargs['pass']).replace("_-_-", "/").replace("____", "?")
			usuario = str(kargs['usuario']).replace("_-_-", "/").replace("____", "?")
		except:
			in_data = {"concept" : "error_get_data"}
			error = True

		if not error:
			l_code = loginCode.objects.get(code = password)
			if l_code.user.username == usuario:
				if l_code.nro_inicio - 1 >= 0:
					l_code.nro_inicio = l_code.nro_inicio - 1
					l_code.save()
				in_data = {"concept" : "success"}
			#in_data = {"concept" : "error_in " + password + " - " + usuario + " - " + l_code.user.username}

		return JsonResponse(in_data)


class getFoto(View):

	def get(self, request, *args, **kargs):
		import magic
		error = False
		foto_url = "static/foto/default.png"
		try:
			usuario = LoginRequest.desencripta(self, str(kargs['username']).replace("_-_-", "/").replace("____", "?"))
			password = LoginRequest.desencripta(self, str(kargs['pass']).replace("_-_-", "/").replace("____", "?"))
			l_code = loginCode.objects.get(code = password)
			zona = str(kargs['zona'])
		except:
			in_data = {"concept" : "error_get_data"}
			error = True

		if not error:
			try:
				if l_code.user.username == usuario:
					foto = FotoZona.objects.filter(Q(login_code = l_code) & Q(zona = zona))
					if len(foto) > 0:
						foto_url = foto[0].imagen.path
				
			except:
				pass

		image_buffer = open(foto_url, "rb").read()
		content_type = magic.from_buffer(foto_url, mime = True)
		response = HttpResponse(image_buffer, content_type = content_type)
		response['Content-Disposition'] = 'attachment; filename="%s"' % os.path.basename(foto_url)
		return response
