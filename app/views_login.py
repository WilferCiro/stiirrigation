from app.models import *
from django.shortcuts import render
from django.views import View
from django.http import HttpResponse, HttpResponseRedirect
from app.forms_login import *
from app.mails import send_an_email

class SingUp(View):
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		form = SingUpForm()

		return render(request, "singup.html", {"form" : form})

	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")

		form = SingUpForm(data = request.POST)
		error = ""
		if form.is_valid():
			apellido = form.cleaned_data["apellido"]
			nombre = form.cleaned_data["nombre"]
			documento = form.cleaned_data["documento"]
			email = form.cleaned_data["email"]
			pass1 = form.cleaned_data["pass1"]
			pass2 = form.cleaned_data["pass2"]
			sexo = form.cleaned_data["sexo"]
			phone1 = form.cleaned_data["phone1"]
			producto = None
			producto = form.cleaned_data["producto"]
			if pass1 != pass2:
				error = "Las contraseñas no coinciden"
			else:
				try:
					exists = User.objects.get(username = documento)
					error = "No se puede registrar este documento, revise si ya está registrado"
				except:
					try:
						new_user = User.objects.create_user(username=documento,
							                 email=email)
						new_user.set_password(pass1)
						new_user.last_name = apellido
						new_user.first_name = nombre
						new_user.documento = documento
						new_user.sexo = sexo
						new_user.celular1 = phone1
						new_user.save()

						asunto = "Registro de nuevo usuario"
						cuerpo = "Se ha registrado un nuevo usuario, " + str(nombre) + " " + str(apellido) + ", solicitando el producto: " + str(producto.titulo) + ", para ver su perfil visita el siguiente link <a href='https://stiirrigation.com/usuarios/view/" + str(new_user.pk) + "'>https://stiirrigation.com/usuarios/view/" + str(new_user.pk) + "</a>"

						mails = []
						for use in User.objects.filter(Q(is_superuser = True)):
							mails.append(use.email)

						send_an_email(asunto, cuerpo, mails)

						return HttpResponseRedirect("/index?logged")
					except Exception as ex:
						print(ex)
						error = "error en el registro"
						pass
		print(form.errors)
		return render(request, "singup.html", {"form" : form, "error" : error})


class ServicioCliente(View):
	def get(self, request, *args, **kvargs):
		success = request.GET.get("success", None)

		form = ServicioClienteForm()

		return render(request, "servicio_cliente.html", {"form" : form, "success" : success})

	def post(self, request, *args, **kvargs):
		form = ServicioClienteForm(request.POST, request.FILES)
		error = ""
		if form.is_valid():
			nombre = form.cleaned_data["nombre"]
			apellido = form.cleaned_data["apellido"]
			email = form.cleaned_data["email"]
			telefono = form.cleaned_data["telefono"]
			asunto = form.cleaned_data["asunto"]
			pregunta = form.cleaned_data["pregunta"]
			tipo = form.cleaned_data["tipo"]
			try:
				imagen = request.FILES["imagen"]
			except:
				imagen = None
			direccion = form.cleaned_data["direccion"]

			form.save()

			ser = servicio_cliente.objects.last()

			asunto = "[Servicio al cliente] " + "[" + str(tipo) + "] " + str(asunto)
			cuerpo = "Mensaje de: " + str(nombre) + " " + str(apellido) + " (Telefono: " + str(telefono) + ", Email: " + str(email) + ", Dirección: " + str(direccion) + ")<br /><br/>" + str(pregunta)
			if ser.imagen != None and ser.imagen != "":
				cuerpo += "<img src='https://www.stiirrigation.com" + str(ser.imagen.url) + "'>"
			mails = []
			for use in User.objects.filter(Q(is_superuser = True)):
				mails.append(use.email)

			send_an_email(asunto, cuerpo, mails)

			return HttpResponseRedirect("/servicio_cliente?success")

		return render(request, "servicio_cliente.html", {"form" : form, "error" : error})


class RecoverPass(View):
	def get_code(self, nro):
		import string, random
		passkey = ''
		for x in range(nro):
			if random.choice([1,2]) == 1:
				passkey += passkey.join(random.choice(string.ascii_uppercase))
			else:
				passkey += passkey.join(random.choice(string.digits))
		return passkey
	
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
				
		return render(request, "recover.html", {})
	
	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		
		username = request.POST.get("username", None)
		error = True
		if username != None:
			try:
				user = User.objects.get(username = username)
				email = user.email
				if user.is_superuser:
					mensaje = "Esta cuenta no puede ser recuperada por este método, por favor contacte con los administradores"
				else:
					mensaje = "Aún no se ha implementado esta opción, (" + str(email) + ")"
				
				if email != "":
					codigo_rec = self.get_code(8)
					while CambioContrasena.objects.filter(Q(codigo = codigo_rec)):
						codigo_rec = self.get_code(5)
						
					cambio_pass = CambioContrasena.objects.filter(usuario = user).last()
					if cambio_pass == None:
						cambio_pass = CambioContrasena(usuario = user)
					cambio_pass.codigo = codigo_rec
					cambio_pass.save()
					
					asunto = "Recuperación de contraseña en stiirrigation"
					cuerpo = "Tu código de recuperación es <b>" + str(codigo_rec) + "</b>, o accede directamente al <a href='http://stiirrigation.com/recover/pass/code?codigo="+ str(codigo_rec) +"'>siguiente enlace</a>"
					send_an_email(asunto, cuerpo, [email])
					
					mensaje = "Se ha enviado un código a tu correo, por favor introdúcelo a continuación"
					
					error = False
					
					return HttpResponseRedirect("/recover/pass/code")
			except User.DoesNotExist:
				mensaje = "No existe usuario con este documento"
			except Exception as e:
				print(e)
				mensaje = "No se pudo recuperar, por favor contacte con los administradores"
		return render(request, "recover.html", {"mensaje" : mensaje, "error" : error})


class RecoverPassCode(View):
	
	def get(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		codigo = request.GET.get("codigo", None)
		
		data_return = {}
		if codigo != None:
			try:
				codigo_bd = CambioContrasena.objects.get(codigo = codigo)
				data_return["code_valid"] = True
				data_return["codigo"] = codigo
				data_return["nombre_usuario"] = codigo_bd.usuario.get_full_name()
			except Exception as e:
				print(e)
				data_return["error"] = True
				data_return["mensaje"] = "El código ingresado no existe"
		
		return render(request, "recover_code.html", data_return)
	
	def post(self, request, *args, **kvargs):
		if request.user.is_authenticated:
			return HttpResponseRedirect("/logout")
		
		tipo = request.POST.get("change_pass", None)
		error = True
		mensaje = "Error"
		if tipo == "change_pass":
			try:
				pass1 = request.POST.get("pass1", None)
				pass2 = request.POST.get("pass2", None)
				codigo = request.POST.get("codigo", None)
				if pass1 == pass2 and pass1 != None and pass1 != "" and codigo != None and codigo != "":
					codigo_bd = CambioContrasena.objects.get(codigo = codigo)
					from datetime import datetime
					now = datetime.now()
					fecha = codigo_bd.fecha_solicitud
					usuario = codigo_bd.usuario
					usuario.set_password(pass1)
					usuario.save()
					codigo_bd.delete()
					error = False
					return HttpResponseRedirect("/index?cambio_pass")
					mensaje = "Ya pasó más de 1 día para el cambio de contraseña, vuelve a solicitar un código"
			except Exception as e:
				print(e)
				mensaje = "No se pudo modificar la contraseña, por favor vuelve a hacer el proceso completo"
		return render(request, "recover_code.html", {"mensaje" : mensaje, "error" : error})

		
		
		
