from .views_main import Main, MainTable
from django.views import View
from django.db.models import Q

from app.models import *
from .form_user import *
from django.utils.translation import ugettext_lazy as _
from app.mails import send_an_email
from django.utils import translation


class UserView(Main, View):
	#TODO: add historic
	template = 'user.html'
	page_title = _("Perfil del usuario")

	def get_data(self, request, kwargs):
		"""
			Gets extra data of the profile
		"""
		self.url = request.path
		user_id = int(kwargs['pk'])
		user_data = User.objects.get(pk = user_id)
		son_mios = False

		if not request.user.is_superuser and user_data != request.user:
			self.can_view_page = False
			return {}
		
		if self.user == user_data:
			son_mios = True
		
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Datos del usuario"), "")

		self._add_js("usuario.js")
		form_user = formUser(instance = user_data)
		form_edit_avatar = editAvatar(instance = user_data)		
		form_change_pass = changePass()		

		add_producto_form = addProductoForm()
		try:
			lang = request.session[translation.LANGUAGE_SESSION_KEY]
		except:
			lang = "es"
		productos = []
		for prod in loginCode.objects.filter(user = user_data):
			name = prod.producto.titulo
			if lang == "en":
				name = name.replace("próximamente", "next")
			url = prod.producto.url_app
			if url == "None" or url == None:
				url = ""
			if request.user.is_superuser:
				cod_lec = readCode.objects.get(login_code = prod)
				cod_esc = writeCode.objects.get(login_code = prod)
				productos.append({"pk" : prod.pk, "producto" : name, "url" : url, "codigo_login" : prod.code, "codigo_lectura" : cod_lec, "codigo_escritura" : cod_esc, "nro_sesion" : prod.nro_inicio})
			else:
				productos.append({"producto" : name, "url" : url, "codigo_login" : prod.code, "nro_sesion" : prod.nro_inicio})
		data_return = {
			"form_edit_user" : form_user,
			"url" : self.url,
			"productos" : productos,
			"user_data" : user_data,
			"add_producto_form" : add_producto_form,
			"form_edit_avatar" : form_edit_avatar,
			"form_change_pass" : form_change_pass,
			"son_mios" : son_mios
		}
		return data_return

	def _editData(self, request):
		"""
			Edit the object data depending of the form
		"""
		tipo_edit = request.POST.get("tipoEdit", None)
		object_id = request.POST.get("object_id", None)
		
		if tipo_edit == "edit_producto":
			try:
				passw = request.POST.get("pass", None)
				if passw == "contraseña_cambio_stiirrigation":
					usuario_id = request.POST.get("usuario", None)
					producto_id = request.POST.get("producto", None)
					usuario = User.objects.get(pk = usuario_id)
					login_code = loginCode.objects.get(Q(pk = producto_id) & Q(user = usuario))
					read_reg = readCode.objects.get(login_code = login_code)
					write_reg = writeCode.objects.get(login_code = login_code)

					key = self.get_code(5)
					while loginCode.objects.filter(Q(code = key)):
						key = self.get_code(5)
					login_code.code = key
					login_code.save()

					key_read = self.get_code(10)
					while readCode.objects.filter(Q(code = key_read)):
						key_read = self.get_code(10)

					read_reg.code = key_read
					read_reg.save()

					key_write = self.get_code(15)
					while readCode.objects.filter(Q(code = key_write)):
						key_write = self.get_code(15)

					write_reg.code = key_write
					write_reg.save()

					mails = [usuario.email]
					asunto = "[Stiirrigation] Modificación de código de producto"
					cuerpo = "Se han modificado el código de ingreso de la aplicación, para acceder al codigo de ingreso a la aplicacion ingresa a: <a href='https://stiirrigation.com/usuarios/view/" + str(usuario.pk) + "'>https://stiirrigation.com/usuarios/view/" + str(usuario.pk) + "</a>"
					send_an_email(asunto, cuerpo, mails)

					return True
			except Exception as e:
				request.session['last_error'] = "Error al editar el producto " + str(e)
		elif tipo_edit == "user_data":			
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user.is_superuser or self.user == user_edit:
					form = formUser(data = request.POST, instance = user_edit)
					if form.is_valid():
						form.save()
						return True
					print(form.errors)
			except Exception as e:
				print(e)
				pass
		elif tipo_edit == "user_avatar":
			try:
				user_edit = User.objects.get(pk = object_id)
				if self.user.is_superuser or self.user == user_edit:
					form = editAvatar(data = request.POST, files = request.FILES, instance = user_edit)
					if form.is_valid():
						form.save()
						return True
					print(form.errors)
			except Exception as e:
				print(e)
				pass
			
		elif tipo_edit == "password":
			try:
				user_edit = User.objects.get(pk = object_id)
				form_edit = changePass(data = request.POST)
				if form_edit.is_valid() and (self.user.is_superuser or self.user == user_edit):
					pass1 = form_edit.cleaned_data["pass1"]
					pass2 = form_edit.cleaned_data["pass2"]
					if pass1 == pass2:
						user_edit.set_password(pass1)
						user_edit.save()
						return True
			except Exception as ex:
				print(ex)
		return False

	def _removeData(self, request):
		"""
			Edit the object data depending of the form
		"""
		try:
			passw = request.POST.get("pass", None)
			if passw == "contraseña_cambio_stiirrigation":
				usuario_id = request.POST.get("usuario", None)
				producto_id = request.POST.get("producto", None)
				usuario = User.objects.get(pk = usuario_id)
				login_code = loginCode.objects.get(Q(pk = producto_id) & Q(user = usuario))
				login_code.delete()
				return True
		except:
			pass
		request.session['last_error'] = "Error al eliminar el producto"
		return False

	def get_code(self, nro):
		import string, random
		passkey = ''
		for x in range(nro):
			if random.choice([1,2]) == 1:
				passkey += passkey.join(random.choice(string.ascii_uppercase))
			else:
				passkey += passkey.join(random.choice(string.digits))
		return passkey

	def _addData(self, request):
		form_edit = addProductoForm(data = request.POST)
		if form_edit.is_valid():
			producto = form_edit.cleaned_data["producto"]
			usuario_id = request.POST.get("usuario", None)
			usuario_reg = User.objects.get(pk = usuario_id)
			if request.user.is_superuser:
				if not loginCode.objects.filter(Q(user = usuario_reg) & Q(producto = producto)).exists():
					key = self.get_code(5)
					while loginCode.objects.filter(Q(code = key)):
						key = self.get_code(5)

					new_login = loginCode(code = key, user = usuario_reg, producto = producto, nro_inicio = 0)
					new_login.save()

					key_read = self.get_code(10)
					while readCode.objects.filter(Q(code = key_read)):
						key_read = self.get_code(10)

					new_read = readCode(login_code = new_login, code = key_read)
					new_read.save()

					key_write = self.get_code(15)
					while readCode.objects.filter(Q(code = key_write)):
						key_write = self.get_code(15)

					new_write = writeCode(login_code = new_login, code = key_write)
					new_write.save()

					mails = [usuario_reg.email]
					asunto = "[Stiirrigation] Registro de producto"
					cuerpo = "Se ha registrado un nuevo producto (" + str(producto.titulo) + "), para acceder al codigo de ingreso a la aplicacion a través del siguiente enlace: <a href='https://stiirrigation.com/usuarios/view/" + str(usuario_reg.pk) + "'>https://stiirrigation.com/usuarios/view/" + str(usuario_reg.pk) + "</a><br /><br />Nombre de usuario: " + str(usuario_reg.username) + "<br />Código de ingreso app: " + str(new_login.code) + "<br />Url App: <a href='" + str(producto.url_app) + "'>" + str(producto.url_app) + "</a>"

					send_an_email(asunto, cuerpo, mails)

					return True
			else:
				asunto = "Petición de nuevo producto"
				cuerpo = "El usuario " + str(usuario_reg.first_name) + " " + str(usuario_reg.last_name) + ", solicita el producto: " + str(producto.titulo) + ", para ver su perfil visita el siguiente <a href='https://stiirrigation.com/usuarios/view/" + str(usuario_reg.pk) + "'>Link</a>"

				mails = []
				for use in User.objects.filter(Q(is_superuser = True)):
					mails.append(use.email)

				send_an_email(asunto, cuerpo, mails)
				return True
		request.session['last_error'] = form_edit.errors
		return False


class UsuariosView(MainTable, View):
	"""
		view of users
	"""
	page_title = _("Usuarios")
	register_name = _("user")
	form_action = "/usuarios/"
	model_object = User
	table_columns =	dict(first_name = _("Nombre"), last_name = _("Apellido"), username = _("Nombre de usuario"), email = _("Correo"), fecha_creacion = _("Fecha de Ingreso"))
	return_edit_columns = ["last_name", "first_name", "email", "celular1", "celular2", "sexo"]
	form_edit = editUser
	form_add = addUser
	delete_register = True

	view_aparte = True

	def _preProccess(self, request):
		if request.user.is_superuser:
			self.can_edit = True
			self.can_delete = True
			self.can_add = True
			self.can_view = True
		else:
			self.can_view_page = False

	def _addData(self, request):
		if self.can_add:
			form_edit = self.form_add(data = request.POST)
			if form_edit.is_valid():
				try:
					username = form_edit.cleaned_data["username"]
					first_name = form_edit.cleaned_data["first_name"]
					last_name = form_edit.cleaned_data["last_name"]
					sexo = form_edit.cleaned_data["sexo"]
					email = form_edit.cleaned_data["email"]
					fecha_nacimiento = form_edit.cleaned_data["fecha_nacimiento"]
					celular1 = form_edit.cleaned_data["celular1"]
					celular2 = form_edit.cleaned_data["celular2"]

					new_user = User.objects.create_user(username=username,
				                         email=email)
					new_user.set_password(username)
					new_user.first_name = first_name
					new_user.last_name = last_name
					new_user.sexo = sexo
					new_user.fecha_nacimiento = fecha_nacimiento
					new_user.celular1 = celular1
					new_user.celular2 = celular2
					new_user.save()
					return True
				except Exception as ex:
					print(str(ex))
					request.session['last_error'] = "Ya existe un usuario con estos datos - " +str(ex)
					return False
			request.session['last_error'] = form_edit.errors
		else:
			request.session['last_error'] = "Usted no tiene permisos para añadir este objeto"
		return False

	def _get_data_tables(self, request):
		self._add_breadcrumb(_("Inicio"), "/")
		self._add_breadcrumb(_("Usuarios"), "")

	def _getFilerTable(self, value, user):
		fil = Q()
		if value != "":
			fil = fil & (Q(last_name__icontains = value) | Q(first_name__icontains = value) | Q(username__icontains=value) | Q(email__icontains = value) | Q(fecha_creacion__icontains = value))
		return fil
