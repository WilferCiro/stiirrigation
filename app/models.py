from django.db import models
from django.contrib.auth.models import AbstractUser
from django.db.models import Q
from ckeditor.fields import RichTextField
from django.utils.translation import ugettext_lazy as _

class Pais(models.Model):
	"""
		Defines the county model
	"""
	nombre = models.CharField(max_length = 50)
	descripcion = models.TextField(null=True, blank=True)

	def __str__(self):
		return self.nombre

class User(AbstractUser):
	"""
		Defines a user model
		@extends of abstractuser
	"""
	fecha_creacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)

	G_MALE = "Masculino"
	G_FEMALE = "Femenino"
	SEX_CHOICES = (
		(G_MALE, _('Masculino')),
		(G_FEMALE, _('Femenino'))
	)
	sexo = models.CharField(
		max_length = 15,
		choices = SEX_CHOICES,
		null = True,
		blank = True
	)

	fecha_nacimiento = models.DateTimeField(null=True, blank=True)
	direccion = models.TextField(null=True, blank = True)
	celular1 = models.CharField(max_length = 100, null=True, blank = True)
	celular2 = models.CharField(max_length = 100, null=True, blank = True)
	red_social1 = models.TextField(null=True, blank = True)
	red_social2 = models.TextField(null=True, blank = True)
	ciudad_residencia = models.CharField(max_length = 100, null=True, blank = True)
	pais_residencia = models.ForeignKey(Pais, on_delete=models.SET_NULL, null=True, blank=True)
	profesion = models.CharField(max_length = 100, null=True, blank = True)
	observaciones = RichTextField(null=True, blank=True)
	avatar = models.ImageField(upload_to='avat/%s', null = True,  blank = True)
	LOCK_CHOICES = (
		("Active", _("Active")),
		("Locked", _('Bloqueado'))
	)
	estado = models.CharField(
		max_length = 10,
		choices = LOCK_CHOICES,
		default = "Active",
	)

	def __str__(self):
		return str(self.get_full_name())

class Categoria(models.Model):
	titulo = models.CharField(max_length = 100, null=True, blank = True)
	descripcion = models.TextField(null=True, blank = True)

class Producto(models.Model):
	"""
		Define el modelo del producto
	"""
	titulo = models.CharField(max_length = 100, null=True, blank = True)
	app_code = models.CharField(max_length = 10, null=True, blank = True)
	url_app = models.CharField(max_length = 200, null=True, blank = True)
	descripcion = RichTextField(null=True, blank=True)
	precio = models.FloatField(default=0.0, null = True, blank = True)
	descuento = models.FloatField(default=0.0, null = True, blank = True)
	categoria = models.ForeignKey(Categoria, on_delete=models.CASCADE, null=True, blank = True)
	tags = models.TextField(null=True, blank = True)
	cantidad = models.IntegerField(default = 0, null=True, blank = True)
	fecha_creacion = models.DateTimeField(auto_now_add = True, null=True, blank=True)
	fecha_modificacion = models.DateTimeField(auto_now = True, null=True, blank=True)

	def __str__(self):
		return str(self.titulo) + " (" + (self.app_code) + ")"


class loginCode(models.Model):
	user = models.ForeignKey(User, on_delete=models.CASCADE, null=True)
	code = models.CharField(max_length = 100, null=True, blank = True)
	producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True)
	nro_inicio = models.IntegerField(default = 0, null=True, blank = True)

	def __str__(self):
		return str(self.user.get_full_name()) + " - " + str(self.code) + " - " + str(self.producto)

class FotoZona(models.Model):
	login_code = models.ForeignKey(loginCode, on_delete=models.CASCADE, null=True)
	zona = models.CharField(max_length = 100, null=True, blank = True)
	imagen = models.ImageField(upload_to='zonas/%s', blank = True, null = True)

	def __str__(self):
		return str(self.login_code) + ", Code: " + str(self.zona)

class readCode(models.Model):
	login_code = models.ForeignKey(loginCode, on_delete=models.CASCADE, null=True)
	code = models.CharField(max_length = 100, null=True, blank = True)

	def __str__(self):
		return str(self.login_code) + ", Code: " + str(self.code)

class writeCode(models.Model):
	login_code = models.ForeignKey(loginCode, on_delete=models.CASCADE, null=True)
	code = models.CharField(max_length = 100, null=True, blank = True)

	def __str__(self):
		return str(self.login_code) + ", Code: " + str(self.code)

## APP CHOICE

class ProductoFoto(models.Model):
	"""
		Define el modelo de las fotos del producto
	"""
	foto = models.ImageField(upload_to='productos/%s', blank = True, null = True)
	producto = models.ForeignKey(Producto, on_delete=models.CASCADE, null=True)


class riego_fieldAutomatico_last(models.Model):
	nombre = models.CharField(max_length = 100, null=True, blank = True)
	login_code = models.ForeignKey(loginCode, on_delete=models.CASCADE, null=True)
	descripcion = models.TextField(null=True, blank = True)
	fecha_ingreso = models.CharField(max_length = 100, null=True, blank = True)
	campo1 = models.CharField(max_length = 200, null=True, blank = True)
	campo2 = models.CharField(max_length = 200, null=True, blank = True)
	campo3 = models.CharField(max_length = 200, null=True, blank = True)
	campo4 = models.CharField(max_length = 200, null=True, blank = True)
	campo5 = models.CharField(max_length = 200, null=True, blank = True)
	campo6 = models.CharField(max_length = 200, null=True, blank = True)
	campo7 = models.CharField(max_length = 200, null=True, blank = True)
	campo8 = models.CharField(max_length = 200, null=True, blank = True)
	campo9 = models.CharField(max_length = 200, null=True, blank = True)
	campo10 = models.CharField(max_length = 200, null=True, blank = True)
	campo11 = models.CharField(max_length = 200, null=True, blank = True)
	days = models.CharField(max_length = 200, null=True, blank = True, default = "5")
	bat1 = models.CharField(max_length = 200, null=True, blank = True)
	bat2 = models.CharField(max_length = 200, null=True, blank = True)
	bat3 = models.CharField(max_length = 200, null=True, blank = True)
	bat4 = models.CharField(max_length = 200, null=True, blank = True)
	bat5 = models.CharField(max_length = 200, null=True, blank = True)
	bat6 = models.CharField(max_length = 200, null=True, blank = True)
	bat7 = models.CharField(max_length = 200, null=True, blank = True)
	bat8 = models.CharField(max_length = 200, null=True, blank = True)
	bat9 = models.CharField(max_length = 200, null=True, blank = True)
	bat10 = models.CharField(max_length = 200, null=True, blank = True)
	bat11 = models.CharField(max_length = 200, null=True, blank = True)
	bat12 = models.CharField(max_length = 200, null=True, blank = True)
	bat13 = models.CharField(max_length = 200, null=True, blank = True)
	bat14 = models.CharField(max_length = 200, null=True, blank = True)
	bat15 = models.CharField(max_length = 200, null=True, blank = True)
	bat16 = models.CharField(max_length = 200, null=True, blank = True)
	bat17 = models.CharField(max_length = 200, null=True, blank = True)
	bat18 = models.CharField(max_length = 200, null=True, blank = True)
	bat19 = models.CharField(max_length = 200, null=True, blank = True)
	bat20 = models.CharField(max_length = 200, null=True, blank = True)
	bat21 = models.CharField(max_length = 200, null=True, blank = True)

	def __str__(self):
		return str(self.nombre)
	

class riego_fieldManual_last(models.Model):
	nombre = models.CharField(max_length = 100, null=True, blank = True)
	login_code = models.ForeignKey(loginCode, on_delete=models.CASCADE, null=True)
	descripcion = models.TextField(null=True, blank = True)
	fecha_ingreso = models.CharField(max_length = 100, null=True, blank = True)
	campo1 = models.CharField(max_length = 200, null=True, blank = True)

	def __str__(self):
		return str(self.nombre)


class servicio_cliente(models.Model):
	nombre = models.CharField(max_length = 100, null=False)
	apellido = models.CharField(max_length = 100, null=False)
	email = models.CharField(max_length = 100, null=False)
	telefono = models.CharField(max_length = 100, null=False)
	direccion = models.CharField(max_length = 200, null=True)
	TIPO_CHOICES = (
		("Pregunta", _("Pregunta")),
		("Sugerencia", _('Sugerencia')),
		("Reporte_fallos", _("Reporte de fallos"))
	)
	tipo = models.CharField(
		max_length = 50,
		choices = TIPO_CHOICES,
		default = "Pregunta",
	)
	imagen = models.ImageField(upload_to='servicio_cliente/%s', blank = True, null = True)
	asunto = models.CharField(max_length = 100, null=False)
	pregunta =models.TextField(null=False)

	def __str__(self):
		return str(self.nombre)

class CambioContrasena(models.Model):
	usuario = models.ForeignKey(User, on_delete=models.SET_NULL, null=True)
	codigo = models.CharField(max_length = 8)
	fecha_solicitud = models.DateTimeField(auto_now = True, null=True, blank=True)
	
	def __str__(self):
		return str(self.codigo)



